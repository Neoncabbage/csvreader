# CSV Reader

## Purpose

This is project is intended to take a CSV and some input from a user and then parse it and insert the CSV into a table in a database.


## What I learned

1. To take input from a user
2. How to parse a CSV
3. Database manipulation with Go


## To Do

1. [x] Handle input from user
2. [x] Parse basic CSV with no quotations
3. [ ] Parse CSV with quotations
4. [ ] Parse an entire directory of CSVs
5. [ ] Build a schema file based on the contents of the CSV, to map correct data type
6. [ ] Add logic to fix non RFC-4180 CSVs (to a certain extent)