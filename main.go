package main

import (
	"fmt"

	"gitlab.org/csvreader/cmd"
)

var conditions = make(map[string]string)

func main() {
	/*
		-needs to ask for input
			-path to the file
			-destination database
			-delimiter, must be able to take in special characters


		-read the csv
			-read the first line as the headers
			-drop table if one with the same name as the CSV exists
			-read the remaining data and insert it into the table

		-optional
			-build a database schema file from the CSV, stored as json data
			-if there's shitty data then it needs to be able to build a regex pattern to remove that data

	*/
	fmt.Println("Welcome to lennox's first try at a CSV reader")
	cmd.Run()

}
