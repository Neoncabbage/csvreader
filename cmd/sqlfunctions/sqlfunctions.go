package sqlfunctions

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

func Query(query string, database string) *sql.Rows {
	db := getDbConnection(database)
	defer db.Close()
	fmt.Println("Query", query)
	rows, err := db.Query(query)

	if err != nil {
		log.Fatalln("Error from query:", err)
	}
	return rows
}

func CreateTable(tableConfig map[string]string, tableName string, database string) *sql.Rows {
	query := "CREATE TABLE " + tableName + "("

	for i, v := range tableConfig {
		query += fmt.Sprintf("%v %v,\n", i, v)
	}
	query +=
		`extraction_id int auto_increment not null primary key,
		extraction_hash varchar(100) not null
		)`

	return Query(query, database)
}

func getDbConnection(database string) *sql.DB {
	db, err := sql.Open("mysql", "root:@/"+database)
	if err != nil {
		log.Fatalln("Error from db connection:", err)
	}
	return db
}

func DatabaseExits(database string, dsn string) bool {
	return false
}

func TableExits(database string, dsn string) bool {
	return false
}
