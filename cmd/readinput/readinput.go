package readinput

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

//https://godoc.org/github.com/spf13/cobra - something to consider
func Main(ri map[string]string) map[string]string {

	for v := range ri {
		switch v {
		case "database":
			ri[v] = getDatabase()
		case "filename":
			ri[v] = getFileName()
		case "delimiter":
			ri[v] = getDelimiter()
		}
	}
	return ri
}

// getDatabase returns a string of the database name from commandline input after validating the input
func getDatabase() string {
	for {
		fmt.Print("Enter the database name: ")
		databaseValid := false

		//prompts the user to enter the database name
		var database string
		_, err := fmt.Scan(&database)
		if err != nil {
			fmt.Println("Error:", err)
		}

		//opens connection to the database to test validity
		database = "test" // remove once finished
		dsn := "root:@/" + database
		fmt.Println("This is the DSN:", dsn) // remove this once testing is finished
		db, err := sql.Open("mysql", dsn)
		defer db.Close()
		if err != nil {
			fmt.Println("Error:", err)
			continue
		}

		_, err = db.Query("select * from information_schema.SCHEMATA where schema_name ='" + database + "'")
		if err != nil {
			fmt.Println("Error:", err)
			continue
		} else {
			databaseValid = true
		}

		if databaseValid {
			fmt.Printf("Successfully connected to %v!\n", database)
			return database
		} else {
			fmt.Println("Unable to connect to", database)
		}
	}
}

// getFileName asks a user for the filepath to the CSV and will keep on asking them for the filename until it is valid
func getFileName() string {
	for {
		fmt.Print("Enter the absolute path to your file: ")
		var fileName string
		_, err := fmt.Scan(&fileName)
		if err != nil {
			fmt.Println("This is the error from reading the string", err)
		}

		fileName = `./MOCK_DATA2.csv` // this is just for ease of testing

		_, err2 := os.Stat(fileName)
		if os.IsNotExist(err2) {
			fmt.Println("This is the error:", err2)
		} else if fileName == "" {
			fmt.Println("Error: Your path is empty")
		} else {
			return fileName
		}
		fmt.Println("This is your input:", fileName)
	}
}

// getDelimiter asks a user for the delimiter used in the CSV and will keep on asking them for the delimiter until it is valid
func getDelimiter() string {
	for {
		fmt.Print("Please enter your delimiter, the only special character allowed is \\t(tab):")
		var delimiter string
		_, err := fmt.Scan(&delimiter)
		if err != nil {
			fmt.Println("Error: ", err)
			continue
		}

		if delimiter == "" {
			fmt.Println("Error: No delimiter has been passed")
			continue
		} else if len(delimiter) > 1 && delimiter == "\\t" {
			fmt.Println("Error: You are not allowed to enter a delimiter longer than one character other than \\t")
		}
		delimiter = "," // remove when done
		return delimiter
	}
}
