package readcsv

import (
	"bufio"
	"crypto/md5"
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.org/csvreader/cmd/sqlfunctions"
)

var settings map[string]string
var tableName string

func Main(ri map[string]string) {
	settings = ri

	//Trim .csv suffix and everything before the /
	tableName = settings["filename"][:strings.LastIndex(settings["filename"], ".csv")]
	tableName = tableName[strings.LastIndex(tableName, "/")+1:]

	file, err := os.Open(settings["filename"])
	if err != nil {
		log.Panicln("Error:", err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	headers := getHeaders(scanner)

	sqlfunctions.CreateTable(createTableConfig(headers), tableName, settings["database"])

	processCsv(scanner, headers)
}

// getHeaders returns a slice strings of the CSV headers
// I've decided to put this into a function just in case I want to change the functionality
func getHeaders(scanner *bufio.Scanner) []string {
	scanner.Scan()
	rawHeaders := scanner.Text()
	headers := strings.Split(rawHeaders, settings["delimiter"])
	fmt.Println(headers)
	return headers
}

//createTableConfig takes a slice of strings which are the columns for the table.
//It will drop the table if it exists and then will return a map of a column name and it's type
func createTableConfig(headers []string) map[string]string {
	fmt.Println("Dropping existing table")
	sqlfunctions.Query("Drop table if exists "+tableName, settings["database"])

	fmt.Println("Creating table")
	config := make(map[string]string, len(headers))
	for _, v := range headers {
		//Needs to be worked on at a later point to be more smart about the column type
		config[v] = "VARCHAR(100)"
	}
	return config
}

//processCsv takes a scanner and a slice of strings a then uses the scanner to read a file, process the data from the CSV into the
//header columns.
func processCsv(scanner *bufio.Scanner, headers []string) {
	var data [][]string
	var row []string
	var buffer string
	var inquote bool
	numHeaders := len(headers)
	delimiter := settings["delimiter"]

	fmt.Println("Processing CSV")
	for scanner.Scan() {
		line := scanner.Text()
		for i, v := range line {
			nextChar := ""
			prevChar := ""

			//This inquote logic needs to be worked on at this point
			if !inquote && v == '"' {
				inquote = true
				continue
			} else if inquote {
				if i < len(line)-1 {
					nextChar = line[i+1 : i+2]
				}
				if i > 0 {
					prevChar = line[i-1 : i]
				}
				if v == '"' && (nextChar == "\"" || prevChar == "\"") {
					continue
				} else {
					inquote = false
				}
			}

			//If you're not in a quote and you're either at the end of the line or find a delimiter add the current buffer to the row
			//Otherwise concat onto the end of the buffer and keep reading
			if (string(v) == delimiter || len(line)-1 == i) && !inquote {
				row = append(row, buffer)
				buffer = ""
			} else {
				if v == '\'' {
					buffer += "''"
				} else {
					buffer += string(v)
				}
			}

			//Once the number of stored values is the same as the number of columns then insert that row into the data variable
			if len(row) == numHeaders {
				data = append(data, row)
				inquote = false
				buffer = ""

				//To avoid the data variable from storing too much data just insert the data once it reaches 1000 lines and
				//clear the data variable
				if len(data) == 1000 {
					insertData(&data, headers)
					data = [][]string{}
				}
				row = []string{}
			}
		}
	}
	//In case there's not 1000 rows of data stored so it will just insert what's left over.
	insertData(&data, headers)
}

//insertData takes a pointer to the data, and a slice of strings that have the headers. It then builds an insert query from those and then
//executes it
func insertData(data *[][]string, headers []string) {
	//builds the first part of the query with the headers
	query := "INSERT INTO " + tableName + "("
	for _, v := range headers {
		query += v + ","
	}
	query += "extraction_hash) VALUES "

	//does a bulk insert by unpacking each entry of variable data and building a string from it.
	for i, v := range *data {
		query += "("
		for _, v2 := range v {
			query += "'" + v2 + "',"
		}

		//md5 hash might be used latter down the line for data validation
		wholeRow := strings.Join(v, "")
		hash := md5.Sum([]byte(wholeRow))

		query += "'" + fmt.Sprintf("%x", hash) + "'" + ")"

		if i < len(*data)-1 {
			query += ",\n"
		}
	}

	rows := sqlfunctions.Query(query, settings["database"])
	fmt.Println(rows)
	fmt.Println("Inserted", len(*data), "rows")
}
