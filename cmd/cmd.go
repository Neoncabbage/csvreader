package cmd

import (
	"gitlab.org/csvreader/cmd/readcsv"
	"gitlab.org/csvreader/cmd/readinput"
)

type Input struct {
	name         string
	input        string
	errorMessage string
}

var requiredInput = map[string]string{
	"filename":  "",
	"database":  "",
	"delimiter": "",
}

func Run() {
	requiredInput = readinput.Main(requiredInput)
	//eventually should be able to wrap this in a go routine and have it multithreading
	readcsv.Main(requiredInput)
}
